import ipaddress
import json
import random


def read_process_file():
    with open('process.json') as json_file:
        data = json.load(json_file)
        amount_to_generate = data["ip"]["generate"]
        networks = []
        for network in data["networks"]:
            networks.append(ipaddress.IPv4Network(network))
        # print(data)
        return amount_to_generate, networks


def generate_ips(amount, networks):
    MAX_REPETITIONS = 50
    repeated = 0
    ips = set()
    while len(ips) < amount and repeated < MAX_REPETITIONS:
        net = random.choice(networks)
        rand = random.randrange(0, net.num_addresses)
        ip = net[rand]
        if ip in ips:
            repeated += 1
        else:
            ips.add(ip)

    if repeated == MAX_REPETITIONS:
        print("Stopping for to many repeated addrasses")
    return ips


def write_to_file(ips):
    file = open("ips.txt", "w")
    for ip in ips:
        file.writelines(str(ip) + "\n")
    file.close()


process_params = read_process_file()
if not process_params[1]:
    print("No networks specified")
else:
    random_ips = generate_ips(process_params[0], process_params[1])
    write_to_file(random_ips)
    print("Info saved in the ips.txt file")
